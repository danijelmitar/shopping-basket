package com.example.shoppingbasket.cart.service;

import com.example.shoppingbasket.cart.dto.CartDto;
import com.example.shoppingbasket.cart.dto.CartTotalDto;

public interface CartService {

    CartDto createCart();

    CartDto addToCart(Long idCart, Long itemDto);

    CartDto removeFromCart(Long idCart, Long idItem);

    CartTotalDto showItemsInCart(long idCart);

}
