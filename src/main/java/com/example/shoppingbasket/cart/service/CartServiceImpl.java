package com.example.shoppingbasket.cart.service;

import com.example.shoppingbasket.cart.Cart;
import com.example.shoppingbasket.cart.dto.CartDto;
import com.example.shoppingbasket.cart.dto.CartTotalDto;
import com.example.shoppingbasket.cart.repository.CartRepository;
import com.example.shoppingbasket.discount.service.DiscountApplierService;
import com.example.shoppingbasket.exception.CartNotFoundException;
import com.example.shoppingbasket.item.Item;
import com.example.shoppingbasket.item.dto.ItemDto;
import com.example.shoppingbasket.item.repository.ItemRepository;
import com.example.shoppingbasket.logging.LoggingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class CartServiceImpl implements CartService {

    private final CartRepository cartRepository;
    private final ItemRepository itemRepository;
    private final DiscountApplierService discountApplierService;
    private final LoggingService loggingService;

    @Override
    public CartDto createCart() {
        Cart cart = this.cartRepository.save(new Cart());

        return CartDto.from(cart);
    }

    @Override
    public CartDto addToCart(Long idCart, Long itemDto) {
        Item item = this.itemRepository.getOne(itemDto);

        Cart cart = addToCart(idCart, item);

        return CartDto.from(cart);
    }

    @Override
    public CartDto removeFromCart(Long idCart, Long idItem) {
        Cart cart = this.cartRepository.getOne(idCart);
        Item item = this.itemRepository.getOne(idItem);

        cart.getItems().remove(item);

        return CartDto.from(cart);
    }

    @Override
    public CartTotalDto showItemsInCart(long idCart) {
        Cart cart = this.cartRepository.getOne(idCart);
        CartDto cartDto = CartDto.from(cart);

        CartTotalDto cartTotalDto = this.discountApplierService.applyDiscount(cartDto);
        cartTotalDto.setTotalSum(calculateTotalSumOfItems(cartTotalDto));

        loggingService.log(cartTotalDto);

        return cartTotalDto;
    }

    private Cart addToCart(Long idCart, Item item) {
        Cart cart = this.cartRepository.findById(idCart)
                .orElseThrow(() -> new CartNotFoundException(String.format("Cart with id %s not found", idCart)));
        cart.getItems().add(item);

        return this.cartRepository.save(cart);
    }

    private BigDecimal calculateTotalSumOfItems(CartTotalDto cartTotalDto) {
        return cartTotalDto.getItems()
                .stream()
                .map(ItemDto::getPrice)
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }

}
