package com.example.shoppingbasket.cart.repository;

import com.example.shoppingbasket.cart.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<Cart, Long> {
}
