package com.example.shoppingbasket.cart.dto;

import com.example.shoppingbasket.cart.Cart;
import com.example.shoppingbasket.item.dto.ItemDto;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class CartDto {

    private Long idCart;
    private List<ItemDto> items;

    public static CartDto from(Cart cart) {
        return CartDto.builder()
                .idCart(cart.getIdCart())
                .items(ItemDto.from(cart.getItems()))
                .build();
    }

}
