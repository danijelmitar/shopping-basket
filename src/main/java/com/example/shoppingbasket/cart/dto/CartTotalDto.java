package com.example.shoppingbasket.cart.dto;

import com.example.shoppingbasket.item.dto.ItemDto;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
public class CartTotalDto {

    private Long idCart;
    @Singular
    private List<ItemDto> items;
    private BigDecimal totalSum;

}
