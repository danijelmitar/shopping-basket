package com.example.shoppingbasket.cart;

import com.example.shoppingbasket.cart.dto.CartDto;
import com.example.shoppingbasket.cart.dto.CartTotalDto;
import com.example.shoppingbasket.cart.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/carts")
@RequiredArgsConstructor
public class CartController {

    private final CartService cartService;

    @PostMapping
    public ResponseEntity<CartDto> createCart() {
        return new ResponseEntity<>(this.cartService.createCart(), HttpStatus.CREATED);
    }

    @PostMapping("/{idCart}")
    public ResponseEntity<CartDto> addToCart(@PathVariable Long idCart, @RequestBody Long idItem) {
        return new ResponseEntity<>(this.cartService.addToCart(idCart, idItem), HttpStatus.OK);
    }

    @DeleteMapping("/{idCart}")
    public ResponseEntity<CartDto> removeFromCart(@PathVariable Long idCart, @RequestBody Long idItem) {
        return new ResponseEntity<>(this.cartService.removeFromCart(idCart, idItem), HttpStatus.OK);
    }

    @GetMapping("/{idCart}")
    public ResponseEntity<CartTotalDto> showItemsInCart(@PathVariable Long idCart) {
        return new ResponseEntity<>(this.cartService.showItemsInCart(idCart), HttpStatus.OK);
    }

}
