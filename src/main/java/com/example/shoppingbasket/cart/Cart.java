package com.example.shoppingbasket.cart;

import com.example.shoppingbasket.item.Item;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Cart {

    @Id
    @Column(name = "id_cart")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCart;

    @ManyToMany
    @JoinTable(
            joinColumns = @JoinColumn(name = "id_cart"),
            inverseJoinColumns = @JoinColumn(name = "id_item")
    )
    private List<Item> items = new ArrayList<>();

}
