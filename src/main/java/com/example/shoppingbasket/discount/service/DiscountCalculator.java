package com.example.shoppingbasket.discount.service;

import com.example.shoppingbasket.discount.dto.DiscountDto;
import com.example.shoppingbasket.item.dto.ItemDto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

class DiscountCalculator {

    private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

    private DiscountDto discountDto;

    public DiscountCalculator(DiscountDto discountDto) {
        this.discountDto = discountDto;
    }

    public List<ItemDto> calculate() {
        List<ItemDto> itemsUsedInDiscount = new ArrayList<>();
        List<ItemDto> triggerItems = discountDto.getTriggerItems();
        ItemDto targetItem = discountDto.getTargetItem();

        targetItem.setPrice(calculateFinalPrice(targetItem));
        targetItem.setDiscount(discountDto.getTargetItemPercentageDiscount().longValue());

        itemsUsedInDiscount.add(targetItem);
        itemsUsedInDiscount.addAll(triggerItems);

        return itemsUsedInDiscount;
    }

    private BigDecimal calculateFinalPrice(ItemDto targetItem) {
        BigDecimal percentageToPay = ONE_HUNDRED.subtract(discountDto.getTargetItemPercentageDiscount());

        return targetItem.getPrice()
                .multiply(percentageToPay).divide(ONE_HUNDRED, RoundingMode.HALF_EVEN);
    }

}
