package com.example.shoppingbasket.discount.service;

import com.example.shoppingbasket.cart.dto.CartDto;
import com.example.shoppingbasket.cart.dto.CartTotalDto;
import com.example.shoppingbasket.discount.Discount;
import com.example.shoppingbasket.discount.dto.DiscountDto;
import com.example.shoppingbasket.item.dto.ItemDto;
import com.example.shoppingbasket.util.ListUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DiscountApplierServiceImpl implements DiscountApplierService {

    private final DiscountService discountService;

    @Override
    public CartTotalDto applyDiscount(CartDto originalCart) {
        CartTotalDto cartTotalDto = CartTotalDto.builder().idCart(originalCart.getIdCart()).build();

        List<ItemDto> itemsAvailableForDiscount = new ArrayList<>(originalCart.getItems());
        List<ItemDto> itemsWithDiscountApplied = new ArrayList<>();

        List<DiscountDto> discounts = findAllApplicableDiscounts(originalCart);
        for (DiscountDto discountDto : discounts) {
            itemsWithDiscountApplied.addAll(calculateDiscount(itemsAvailableForDiscount, discountDto));
        }

        cartTotalDto.setItems(itemsWithDiscountApplied);
        cartTotalDto.getItems().addAll(itemsAvailableForDiscount);

        return cartTotalDto;
    }

    private List<DiscountDto> findAllApplicableDiscounts(CartDto cartDto) {
        List<Discount> discounts = this.discountService.findAll();

        return DiscountDto.from(discounts).stream()
                .filter(discountDto -> isDiscountApplicable(discountDto, cartDto.getItems()))
                .collect(Collectors.toList());
    }

    private List<ItemDto> calculateDiscount(List<ItemDto> itemsAvailableForDiscount, DiscountDto discountDto) {
        List<ItemDto> itemsWithDiscountApplied = new ArrayList<>();
        while (isDiscountApplicable(discountDto, itemsAvailableForDiscount)) {
            itemsWithDiscountApplied.addAll(calculateDiscount(discountDto));

            itemsAvailableForDiscount.remove(discountDto.getTargetItem());
            discountDto.getTriggerItems().forEach(itemsAvailableForDiscount::remove);
        }

        return itemsWithDiscountApplied;
    }

    private boolean isDiscountApplicable(DiscountDto discountDto, List<ItemDto> itemsToProcess) {
        List<ItemDto> discountItems = new ArrayList<>(discountDto.getTriggerItems());
        discountItems.add(discountDto.getTargetItem());

        return ListUtil.containsAllWithCardinality(itemsToProcess, discountItems);
    }

    private List<ItemDto> calculateDiscount(DiscountDto discountDto) {
        return new DiscountCalculator(discountDto).calculate();
    }

}
