package com.example.shoppingbasket.discount.service;

import com.example.shoppingbasket.cart.dto.CartDto;
import com.example.shoppingbasket.cart.dto.CartTotalDto;

public interface DiscountApplierService {

    CartTotalDto applyDiscount(CartDto cart);

}
