package com.example.shoppingbasket.discount.service;

import com.example.shoppingbasket.discount.Discount;
import com.example.shoppingbasket.discount.repository.DiscountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class DiscountServiceImpl implements DiscountService {

    private final DiscountRepository discountRepository;

    @Override
    @Cacheable(value = "discounts")
    public List<Discount> findAll() {
        return this.discountRepository.findAll();
    }

}
