package com.example.shoppingbasket.discount.service;

import com.example.shoppingbasket.discount.Discount;

import java.util.List;

public interface DiscountService {

    List<Discount> findAll();

}
