package com.example.shoppingbasket.discount.dto;

import com.example.shoppingbasket.discount.Discount;
import com.example.shoppingbasket.item.dto.ItemDto;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class DiscountDto {

    private ItemDto targetItem;
    private List<ItemDto> triggerItems;
    private BigDecimal targetItemPercentageDiscount;

    public static DiscountDto from(Discount discount) {
        return DiscountDto.builder()
                .targetItem(ItemDto.from(discount.getTargetItem()))
                .triggerItems(ItemDto.from(discount.getTriggerItems()))
                .targetItemPercentageDiscount(new BigDecimal(discount.getTargetItemPercentageDiscount()))
                .build();
    }

    public static List<DiscountDto> from(List<Discount> discounts) {
        return discounts.stream().map(DiscountDto::from).collect(Collectors.toList());
    }

}
