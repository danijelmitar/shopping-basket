package com.example.shoppingbasket.discount;

import com.example.shoppingbasket.item.Item;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
public class Discount implements Serializable {

    @Id
    @Column(name = "id_discount")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idDiscount;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            joinColumns = @JoinColumn(name = "id_discount"),
            inverseJoinColumns = @JoinColumn(name = "id_item")
    )
    private List<Item> triggerItems;

    @ManyToOne
    @JoinColumn(name = "id_item")
    private Item targetItem;

    @Column(name = "discount_percentage")
    private Integer targetItemPercentageDiscount;

}
