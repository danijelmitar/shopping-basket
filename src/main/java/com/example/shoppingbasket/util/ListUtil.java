package com.example.shoppingbasket.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ListUtil {

    /**
     *
     * @return true if @param list contains all of the elements of @param subsetList including number of occurrences
     * (duplicates), false otherwise. List need to have at least same number of element occurrences.
     * For, example:
     * - returns false if list=["A", "B", "C"] and subsetList=["A", "A"]. Note that List.containsAll() would return true
     * - returns true if list=["A", "A", "B", "C"] and subsetList=["A", "A"]
     * - returns true if list=["A", "A", "A", "B", "C"] and subsetList=["A", "A"]
     */
    public static <T> boolean containsAllWithCardinality(List<T> list, List<T> subsetList) {
        Map<T, Integer> listElementOccurrences = calculateEachElementOccurrences(list);
        Map<T, Integer> subsetListElementOccurrences = calculateEachElementOccurrences(subsetList);

        for (Entry<T, Integer> entry : subsetListElementOccurrences.entrySet()) {
            if (!containsElementWithCardinality(listElementOccurrences, entry)) {
                return false;
            }
        }

        return true;
    }

    private static <T> Map<T, Integer> calculateEachElementOccurrences(List<T> list) {
        Map<T, Integer> elementOccurrences = new HashMap<>();

        for (T element : list) {
            int occurrences = elementOccurrences.containsKey(element) ? elementOccurrences.get(element) + 1 : 1;
            elementOccurrences.put(element, occurrences);
        }

        return elementOccurrences;
    }

    private static <T> boolean containsElementWithCardinality(Map<T, Integer> counterA1, Entry<T, Integer> entry) {
        T key = entry.getKey();
        Integer count = entry.getValue();

        return counterA1.containsKey(key) && counterA1.get(key) >= count;
    }

}
