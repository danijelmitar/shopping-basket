package com.example.shoppingbasket.logging;

import com.example.shoppingbasket.cart.dto.CartTotalDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ConsoleLoggingServiceImpl implements LoggingService {

    @Override
    public void log(CartTotalDto cartTotalDto) {
        log.info("Items inside cart: {}", cartTotalDto);
    }

}
