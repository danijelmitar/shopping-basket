package com.example.shoppingbasket.logging;

import com.example.shoppingbasket.cart.dto.CartTotalDto;

public interface LoggingService {

    void log(CartTotalDto cartTotalDto);

}
