package com.example.shoppingbasket.config;

import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizeConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;

import javax.annotation.PreDestroy;

@Slf4j
@Configuration
@EnableCaching
@RequiredArgsConstructor
public class CacheConfig {

    public static final String LOCAL = "local";

    private final Environment env;

    @Value("${spring.application.name}")
    private String applicationName;

    @PreDestroy
    public void destroy() {
        Hazelcast.shutdownAll();
    }

    @Bean
    public HazelcastCacheManager cacheManager(HazelcastInstance hazelcastInstance) {
        return new HazelcastCacheManager(hazelcastInstance);
    }

    @Bean
    public HazelcastInstance hazelcastInstance() {
        HazelcastInstance hazelcastInstance = Hazelcast.getHazelcastInstanceByName(applicationName);

        if (hazelcastInstance != null) {
            log.debug("Hazelcast already initialized");

            return hazelcastInstance;
        }

        Config config = new Config();

        config.setInstanceName(applicationName);
        config.getMapConfigs().put("default", initializeDefaultMapConfig());

        Profiles local = Profiles.of(LOCAL);
        if (env.acceptsProfiles(local)) {
            removeMulticastAutoConfiguration(config);
        }

        return Hazelcast.newHazelcastInstance(config);
    }

    private MapConfig initializeDefaultMapConfig() {
        MapConfig mapConfig = new MapConfig();

        mapConfig.setBackupCount(0);
        mapConfig.setEvictionPolicy(EvictionPolicy.LRU);
        mapConfig.setMaxSizeConfig(new MaxSizeConfig(0, MaxSizeConfig.MaxSizePolicy.USED_HEAP_SIZE));

        return mapConfig;
    }

    private void removeMulticastAutoConfiguration(Config config) {
        config.getNetworkConfig().getJoin().getAwsConfig().setEnabled(false);
        config.getNetworkConfig().getJoin().getMulticastConfig().setEnabled(false);
        config.getNetworkConfig().getJoin().getTcpIpConfig().setEnabled(false);
    }

}

