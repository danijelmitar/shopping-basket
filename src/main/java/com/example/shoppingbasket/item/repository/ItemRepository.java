package com.example.shoppingbasket.item.repository;

import com.example.shoppingbasket.item.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<Item, Long> {
}
