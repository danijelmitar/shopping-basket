package com.example.shoppingbasket.item.dto;

import com.example.shoppingbasket.item.Item;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class ItemDto {

    @NonNull
    private final Long idItem;
    private String name;
    private BigDecimal price;
    private long discount;

    public static ItemDto from(Item item) {
        return ItemDto.builder()
                .idItem(item.getIdItem())
                .name(item.getName())
                .price(item.getPrice())
                .build();
    }

    public static List<ItemDto> from(List<Item> items) {
        return items.stream().map(ItemDto::from).collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ItemDto itemDto = (ItemDto) o;

        return idItem.equals(itemDto.idItem);
    }

    @Override
    public int hashCode() {
        return idItem.hashCode();
    }
}
