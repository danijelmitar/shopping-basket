package com.example.shoppingbasket.cart;

import com.example.shoppingbasket.cart.dto.CartDto;
import com.example.shoppingbasket.cart.dto.CartTotalDto;
import com.example.shoppingbasket.cart.service.CartService;
import com.example.shoppingbasket.item.dto.ItemDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.payload.JsonFieldType.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@WebMvcTest(CartController.class)
@ActiveProfiles("ci-testing")
public class CartControllerShould {

    public static final String CARTS_ENDPOINT = "/api/v1/carts";

    public static final long ID_CART = 1L;

    public static final long ID_ITEM_BUTTER = 1L;
    public static final long ID_ITEM_MILK = 2L;
    public static final long ID_ITEM_BREAD = 3L;
    public static final long UNEXISTING_ID_ITEM = 4L;

    public static final String ITEM_NAME_BUTTER = "Butter";
    public static final String ITEM_NAME_MILK = "Milk";
    public static final String ITEM_NAME_BREAD = "Bread";

    public static final BigDecimal ITEM_PRICE_BUTTER = new BigDecimal("0.80");
    public static final BigDecimal ITEM_PRICE_MILK = new BigDecimal("1.15");
    public static final BigDecimal ITEM_PRICE_BREAD = new BigDecimal("1.00");
    public static final BigDecimal TOTAL_SUM_NO_DISCOUNT = new BigDecimal("2.95");
    public static final BigDecimal TOTAL_SUM_BREAD_50_PERCENT_OFF = new BigDecimal("3.10");
    public static final BigDecimal TOTAL_SUM_MILK_FOR_FREE = new BigDecimal("3.45");
    public static final BigDecimal TOTAL_SUM_MULTIPLE_DISCOUNTS = new BigDecimal("9.00");
    public static final BigDecimal TOTAL_SUM_THREE_MILKS = new BigDecimal("3.45");

    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");

    private RestDocumentationResultHandler documentationResultHandler;

    private MockMvc mockMvc;

    @MockBean
    private CartService cartService;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @Before
    public void setUp() {
        this.documentationResultHandler =
                document(
                        "{method-name}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint())
                );

        this.mockMvc =
                webAppContextSetup(this.context)
                        .apply(documentationConfiguration(this.restDocumentation))
                        .alwaysDo(documentationResultHandler)
                        .build();
    }

    @Test
    public void provide_endpoint_to_create_cart() throws Exception {
        CartDto cartDto = buildCart();

        when(this.cartService.createCart()).thenReturn(cartDto);

        this.mockMvc
                .perform(
                        post(CARTS_ENDPOINT).accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isCreated())
                .andExpect(content().string(this.objectMapper.writeValueAsString(cartDto)))
                .andDo(
                        this.documentationResultHandler.document(
                                responseFields(
                                        fieldWithPath("idCart").description("Cart unique id").type(NUMBER),
                                        fieldWithPath("items[]").description("Items inside cart, empty array upon cart creation").type(ARRAY)
                                )
                        )
                );

        verify(this.cartService, times(1)).createCart();
    }

    @Test
    public void provide_endpoint_to_add_item_to_cart() throws Exception {
        ItemDto itemDto = buildButter();
        CartDto cartDto = buildCart();
        cartDto.setItems(Collections.singletonList(itemDto));

        when(this.cartService.addToCart(cartDto.getIdCart(), itemDto.getIdItem())).thenReturn(cartDto);

        this.mockMvc
                .perform(
                        post(CARTS_ENDPOINT + "/" + cartDto.getIdCart())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(this.objectMapper.writeValueAsString(itemDto.getIdItem()))
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(content().string(this.objectMapper.writeValueAsString(cartDto)))
                .andDo(
                        this.documentationResultHandler.document(
                                responseFields(
                                        fieldWithPath("idCart").description("Cart unique id").type(NUMBER),
                                        fieldWithPath("items[]").description("Items inside the cart").type(ARRAY),
                                        fieldWithPath("items[].idItem").description("Item unique id").type(NUMBER),
                                        fieldWithPath("items[].name").description("Item name").type(STRING),
                                        fieldWithPath("items[].discount").description("Discount percentage applied to item price").type(NUMBER),
                                        fieldWithPath("items[].price").description("Item price").type(NUMBER)
                                )
                        )
                );

        verify(this.cartService, times(1)).addToCart(cartDto.getIdCart(), itemDto.getIdItem());
    }

    @Test
    public void provide_endpoint_to_remove_item_from_cart() throws Exception {
        ItemDto itemDto = buildButter();
        CartDto cartDto = buildCart();
        cartDto.setItems(Collections.singletonList(itemDto));

        when(this.cartService.removeFromCart(cartDto.getIdCart(), itemDto.getIdItem())).thenReturn(cartDto);

        this.mockMvc
                .perform(
                        delete(CARTS_ENDPOINT + "/" + cartDto.getIdCart())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(this.objectMapper.writeValueAsString(itemDto.getIdItem()))
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(content().string(this.objectMapper.writeValueAsString(cartDto)))
                .andDo(
                        this.documentationResultHandler.document(
                                responseFields(
                                        fieldWithPath("idCart").description("Cart unique id").type(NUMBER),
                                        fieldWithPath("items[]").description("Items inside the cart").type(ARRAY),
                                        fieldWithPath("items[].idItem").description("Item unique id").type(NUMBER),
                                        fieldWithPath("items[].name").description("Item name").type(STRING),
                                        fieldWithPath("items[].discount").description("Discount percentage applied to item price").type(NUMBER),
                                        fieldWithPath("items[].price").description("Item price").type(NUMBER)
                                )
                        )
                );

        verify(this.cartService, times(1)).removeFromCart(cartDto.getIdCart(), itemDto.getIdItem());
    }

    @Test
    public void provide_endpoint_to_list_items_in_cart() throws Exception {
        CartTotalDto cartTotalDto = buildCartTotal();

        when(this.cartService.showItemsInCart(ID_CART)).thenReturn(cartTotalDto);

        this.mockMvc
                .perform(
                        get(CARTS_ENDPOINT + "/" + ID_CART)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(content().string(this.objectMapper.writeValueAsString(cartTotalDto)))
                .andDo(
                        this.documentationResultHandler.document(
                                responseFields(
                                        fieldWithPath("idCart").description("Cart unique id").type(NUMBER),
                                        fieldWithPath("items[]").description("Items inside the cart").type(ARRAY),
                                        fieldWithPath("items[].idItem").description("Item unique id").type(NUMBER),
                                        fieldWithPath("items[].name").description("Item name").type(STRING),
                                        fieldWithPath("items[].price").description("Item price").type(NUMBER),
                                        fieldWithPath("items[].discount").description("Discount percentage applied to item price").type(NUMBER),
                                        fieldWithPath("totalSum").description("Total price of items inside cart").type(NUMBER)
                                )
                        )
                );

        verify(this.cartService, times(1)).showItemsInCart(ID_CART);
    }

    private CartDto buildCart() {
        return CartDto.builder()
                .idCart(ID_CART)
                .items(Collections.emptyList())
                .build();
    }

    private CartTotalDto buildCartTotal() {
        ItemDto butter = buildItem(ID_ITEM_BUTTER, ITEM_NAME_BUTTER, ITEM_PRICE_BUTTER);
        ItemDto milk = buildItem(ID_ITEM_MILK, ITEM_NAME_MILK, ITEM_PRICE_MILK);
        ItemDto bread = buildItem(ID_ITEM_BREAD, ITEM_NAME_BREAD, ITEM_PRICE_BREAD);

        return CartTotalDto.builder()
                .idCart(ID_CART)
                .items(Arrays.asList(butter, milk, bread))
                .totalSum(TOTAL_SUM_NO_DISCOUNT)
                .build();
    }

    private ItemDto buildButter() {
        return ItemDto.builder()
                .idItem(ID_ITEM_BUTTER)
                .name(ITEM_NAME_BUTTER)
                .price(ITEM_PRICE_BUTTER)
                .build();
    }

    private ItemDto buildItem(Long idItem, String name, BigDecimal price) {
        return ItemDto.builder()
                .idItem(idItem)
                .name(name)
                .price(price)
                .build();
    }

}
