package com.example.shoppingbasket.cart;

import com.example.shoppingbasket.cart.dto.CartDto;
import com.example.shoppingbasket.cart.dto.CartTotalDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import static com.example.shoppingbasket.cart.CartControllerShould.*;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("ci-testing")
public class CartITShould {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void create_cart() {
        ResponseEntity<CartDto> response = this.restTemplate.exchange(
                CARTS_ENDPOINT,
                HttpMethod.POST,
                HttpEntity.EMPTY,
                CartDto.class
        );

        assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.CREATED);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getIdCart()).isNotNull();
        assertThat(response.getBody().getItems()).isEmpty();
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:sql/teardown.sql")
    public void return_404_when_adding_item_to_unexisting_cart() {
        ResponseEntity<CartDto> response = this.restTemplate.postForEntity(
                CARTS_ENDPOINT + "/" + ID_CART,
                UNEXISTING_ID_ITEM,
                CartDto.class
        );

        assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.NOT_FOUND);
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/cart/add_item_to_cart.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:sql/teardown.sql")
    public void add_item_to_cart() {
        ResponseEntity<CartDto> response = this.restTemplate.postForEntity(
                CARTS_ENDPOINT + "/" + ID_CART,
                ID_ITEM_BUTTER,
                CartDto.class
        );

        assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getItems()).hasSize(1);
        assertThat(response.getBody().getItems().get(0).getIdItem()).isEqualTo(ID_ITEM_BUTTER);
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/cart/remove_item_from_cart.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:sql/teardown.sql")
    public void remove_item_from_cart() {
        ResponseEntity<CartDto> response = this.restTemplate.exchange(
                CARTS_ENDPOINT + "/" + ID_CART,
                HttpMethod.DELETE,
                new HttpEntity<>(ID_ITEM_BUTTER),
                CartDto.class
        );

        assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getItems()).hasSize(1);
        assertThat(response.getBody().getItems().get(0).getIdItem()).isEqualTo(ID_ITEM_BUTTER);
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/cart/show_items_in_cart.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:sql/teardown.sql")
    public void show_items_in_cart() {
        ResponseEntity<CartTotalDto> response = this.restTemplate.exchange(
                CARTS_ENDPOINT + "/" + ID_CART,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                CartTotalDto.class
        );

        assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getIdCart()).isEqualTo(ID_CART);
        assertThat(response.getBody().getItems()).hasSize(3);

        assertThat(response.getBody().getItems().get(0).getIdItem()).isEqualTo(ID_ITEM_BUTTER);
        assertThat(response.getBody().getItems().get(0).getName()).isEqualTo(ITEM_NAME_BUTTER);
        assertThat(response.getBody().getItems().get(0).getPrice().toString()).isEqualTo(ITEM_PRICE_BUTTER.toString());

        assertThat(response.getBody().getItems().get(1).getIdItem()).isEqualTo(ID_ITEM_MILK);
        assertThat(response.getBody().getItems().get(1).getName()).isEqualTo(ITEM_NAME_MILK);
        assertThat(response.getBody().getItems().get(1).getPrice().toString()).isEqualTo(ITEM_PRICE_MILK.toString());

        assertThat(response.getBody().getItems().get(2).getIdItem()).isEqualTo(ID_ITEM_BREAD);
        assertThat(response.getBody().getItems().get(2).getName()).isEqualTo(ITEM_NAME_BREAD);
        assertThat(response.getBody().getItems().get(2).getPrice()).isEqualTo(ITEM_PRICE_BREAD.toString());

        assertThat(response.getBody().getTotalSum().toString()).isEqualTo(TOTAL_SUM_NO_DISCOUNT.toString());
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/cart/apply_50_percent_discount_to_bread_when_two_butters_in_cart.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:sql/teardown.sql")
    public void apply_50_percent_discount_to_bread_when_two_butters_in_cart() {
        ResponseEntity<CartTotalDto> response = this.restTemplate.exchange(
                CARTS_ENDPOINT + "/" + ID_CART,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                CartTotalDto.class
        );

        assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getIdCart()).isEqualTo(ID_CART);
        assertThat(response.getBody().getItems()).hasSize(4);

        assertThat(response.getBody().getTotalSum().toString()).isEqualTo(TOTAL_SUM_BREAD_50_PERCENT_OFF.toString());
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/cart/give_one_milk_for_free_when_four_milks_in_basket.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:sql/teardown.sql")
    public void give_one_milk_for_free_when_four_milks_in_basket() {
        ResponseEntity<CartTotalDto> response = this.restTemplate.exchange(
                CARTS_ENDPOINT + "/" + ID_CART,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                CartTotalDto.class
        );

        assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getIdCart()).isEqualTo(ID_CART);
        assertThat(response.getBody().getItems()).hasSize(4);

        assertThat(response.getBody().getTotalSum().toString()).isEqualTo(TOTAL_SUM_MILK_FOR_FREE.toString());
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/cart/apply_multiple_discounts_if_applicable.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:sql/teardown.sql")
    public void apply_multiple_discounts_if_applicable() {
        ResponseEntity<CartTotalDto> response = this.restTemplate.exchange(
                CARTS_ENDPOINT + "/" + ID_CART,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                CartTotalDto.class
        );

        assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getIdCart()).isEqualTo(ID_CART);
        assertThat(response.getBody().getItems()).hasSize(11);

        assertThat(response.getBody().getTotalSum().toString()).isEqualTo(TOTAL_SUM_MULTIPLE_DISCOUNTS.toString());
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/cart/do_not_apply_discount_when_not_enough_items.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:sql/teardown.sql")
    public void do_not_apply_discount_when_not_enough_items_for_discount() {
        ResponseEntity<CartTotalDto> response = this.restTemplate.exchange(
                CARTS_ENDPOINT + "/" + ID_CART,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                CartTotalDto.class
        );

        assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getIdCart()).isEqualTo(ID_CART);
        assertThat(response.getBody().getItems()).hasSize(3);

        assertThat(response.getBody().getTotalSum().toString()).isEqualTo(TOTAL_SUM_THREE_MILKS.toString());
    }

}
