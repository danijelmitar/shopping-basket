package com.example.shoppingbasket.cart.service;

import com.example.shoppingbasket.cart.repository.CartRepository;
import com.example.shoppingbasket.discount.service.DiscountApplierService;
import com.example.shoppingbasket.exception.CartNotFoundException;
import com.example.shoppingbasket.item.repository.ItemRepository;
import com.example.shoppingbasket.logging.LoggingService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityNotFoundException;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class CartServiceShould {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private CartService cartService;

    @MockBean
    private CartRepository cartRepository;

    @MockBean
    private ItemRepository itemRepository;

    @MockBean
    private DiscountApplierService discountApplierService;

    @MockBean
    private LoggingService loggingService;

    @Before
    public void beforeClass() {
        cartService = new CartServiceImpl(cartRepository, itemRepository, discountApplierService, loggingService);
    }

    @Test
    public void raise_exception_when_cart_not_found() {
        when(this.cartRepository.getOne(anyLong())).thenThrow(new EntityNotFoundException());

        exception.expect(CartNotFoundException.class);
        exception.expectMessage("Cart with id 1 not found");

        this.cartService.addToCart(1L, 1L);

        verify(this.cartRepository, times(1)).getOne(anyLong());
    }

}