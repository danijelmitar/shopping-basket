package com.example.shoppingbasket.util;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ListUtilShould {

    @Test
    public void return_false_when_list_contains_all_elements_of_another_list_but_cardinality_doesnt_match() {
        List<String> list = Arrays.asList("A", "B", "C");
        List<String> possibleSubsetList = Arrays.asList("A", "A");

        boolean containsAllWithCardinality = ListUtil.containsAllWithCardinality(list, possibleSubsetList);

        assertThat(containsAllWithCardinality).isFalse();
    }

    @Test
    public void return_true_when_list_contains_all_elements_of_another_list_and_cardinality_matches() {
        List<String> list = Arrays.asList("A", "A", "B", "C");
        List<String> possibleSubsetList = Arrays.asList("A", "A");

        boolean containsAllWithCardinality = ListUtil.containsAllWithCardinality(list, possibleSubsetList);

        assertThat(containsAllWithCardinality).isTrue();
    }

    @Test
    public void return_true_when_list_contains_all_elements_of_another_list_and_list_has_greater_cardinality() {
        List<String> list = Arrays.asList("A", "A", "A", "B", "C");
        List<String> possibleSubsetList = Arrays.asList("A", "A");

        boolean containsAllWithCardinality = ListUtil.containsAllWithCardinality(list, possibleSubsetList);

        assertThat(containsAllWithCardinality).isTrue();
    }

}