package com.example.shoppingbasket;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

// Naming test classes and methods: https://codurance.com/2014/12/13/naming-test-classes-and-methods/
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("ci-testing")
public class ShoppingBasketApplicationITShould {

    @Test
    public void load_context() {
    }

}
