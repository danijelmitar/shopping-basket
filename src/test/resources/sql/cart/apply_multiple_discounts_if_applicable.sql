INSERT INTO cart VALUES (1);

INSERT INTO item VALUES (1, 'Butter', '0.80');
INSERT INTO item VALUES (2, 'Milk', '1.15');
INSERT INTO item VALUES (3, 'Bread', '1.00');

INSERT INTO cart_items VALUES (1, 1);
INSERT INTO cart_items VALUES (1, 1);
INSERT INTO cart_items VALUES (1, 3);
INSERT INTO cart_items VALUES (1, 2);
INSERT INTO cart_items VALUES (1, 2);
INSERT INTO cart_items VALUES (1, 2);
INSERT INTO cart_items VALUES (1, 2);
INSERT INTO cart_items VALUES (1, 2);
INSERT INTO cart_items VALUES (1, 2);
INSERT INTO cart_items VALUES (1, 2);
INSERT INTO cart_items VALUES (1, 2);

INSERT INTO discount (id_discount, id_item, discount_percentage) VALUES (1, 3, 50);
INSERT INTO discount (id_discount, id_item, discount_percentage) VALUES (2, 2, 100);

INSERT INTO discount_trigger_items VALUES (1, 1);
INSERT INTO discount_trigger_items VALUES (1, 1);
INSERT INTO discount_trigger_items VALUES (2, 2);
INSERT INTO discount_trigger_items VALUES (2, 2);
INSERT INTO discount_trigger_items VALUES (2, 2);